package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;
import com.agiletestingalliance.*;

public class AboutCPDOFTest extends Mockito{

    @Test
    public void testDesc() throws Exception {

	StringBuilder stringWriter = new StringBuilder(new AboutCPDOF().desc());
        System.out.print(stringWriter.toString());
        assertTrue(stringWriter.toString().contains("CP-DOF certification"));        
    }
}
